import mongoose from 'mongoose'

const ResultCache = mongoose.Schema({
    url: String,
    main_url: String,
    search: String,
    description: String,
    ocurrences: Number,
    date: Date,
    visited: Boolean


});

module.exports = mongoose.model('resultsCache', ResultCache);