import mongoose from 'mongoose'

const SearchCache = mongoose.Schema({
    url: String,
    search: String,
    date: Date


});

export default mongoose.model('searchsCache', SearchCache);