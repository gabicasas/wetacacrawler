import  path  from 'path';
//const express = require('express');
import express from 'express'
 import  { morgan} from 'morgan';

 import mongoose from 'mongoose';


 mongoose.connect('mongodb://localhost/wetacaCrawler')
 .then(db => console.log("mongodb funcionando") )
 .catch(error => console.error(error))

const app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

const indexRoutes= require("./routes/index")

app.set('port', process.env.PORT || 3000);
//Log


//Routing

app.use('/', indexRoutes);
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
  });