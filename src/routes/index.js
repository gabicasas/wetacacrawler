const express = require('express');
const router = express.Router();
import bodyParser from 'body-parser'; 
import Crawler from '../crawler/index.js';
const ResultCache =  require('../model/resultCache');


router.use(bodyParser.urlencoded({
    extended: true
  }));

router.use(bodyParser.json());
router.get('/', (req,res) => {
    

    res.render('test')

})

router.get('/test',async (req,res) => {
    var aux="https:\/\/www.ideal.es\/*";
        let result=await ResultCache.find({url:new RegExp(aux)});
    res.send(result);

  
})


router.post('/search',async (req,res) => {
    console.log(req.body);
    let url=req.body.url
    let search=req.body.search
   
    let craw=new Crawler(url,search);
    let fromCache=await craw.initialize();
    if(fromCache){
    let response= await craw.doCraw();
   
   
    res.render('result',{'response':response,'fromCache':fromCache, 'url':url, 'search':search});



    
    }else{
        res.render('result',{'response':[],'fromCache':fromCache, 'url':url, 'search':search}); 
         await craw.doCraw();
    }

    craw.destroy()
})

router.post('/',async (req,res) => {
    console.log(req.body);
    let url=req.body.url
    let search=req.body.search
   
    let craw=new Crawler(url,search);
    await craw.initialize();
    //let response= await craw.launch(url);
    let response= await craw.doCraw();
   
    res.json(response);



    craw.destroy()


})

module.exports=router