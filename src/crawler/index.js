import puppeteer from 'puppeteer'
import SearchCache from '../model/searchCache';
import ResultCache from '../model/resultCache';

/**
 * Clase principal de encargada de hacer las tareas de crawling.
 * Utiliza puppeteer para ello. El mecanismo de acción consiste en buscar
 * todos los links de la pagina via XPATH e ir navegadonlos.
 * De manera opcional se buscará un token de busqueda en el texto adyacente al link para ver 
 * si buscamos solo links relacinados a este texto
 * Dado que este proceso es lento se almacenarán los resultados en una caché en mongodb
 */

export default  class Crawler{

    static ttlCrawler=process.env.TIME_CACHE || 24*60*60*1000; //1 dia de cache

    static PUPPETEER_OPTS = {
        headless: true,
        slowMo: { default: 300, click: 200, keyup: 10 },
        devtools: false
      };

    /**
     * Construye el crawlr asociado a una url determinada y un token de busqueda
     * @param {String} url 
     * @param {String} token 
     */  
    constructor(url, token){
       
       // this.browserLoaded=false;
        this.url=url;
        this.token=token;
        this.browser=null;
        this.obtainFromCache=false;
       
       

    }

    /**
     * Inicializa el crawler abriendo el browser de puppeteer si los datos a buscar no estan en la cache
     */

    async initialize(){
        let filterObj={url:this.url,search:this.token}
      
        let sc=await SearchCache.findOne(filterObj);
      
         if(!sc){
             console.log("No hemos encontrado");
             let aux={...filterObj,date:new Date()};
             let scSave= new SearchCache(aux);
             await scSave.save()
          }else{
             console.log("Ya hay")
             if((new Date()).getMilliseconds()-sc.date.getMilliseconds()>Crawler.ttlCrawler){
                await ResultCache.remove({main_url:sc.url});
                await SearchCache.remove({_id:sc._id});
               
             }else
                this.obtainFromCache=true;
          }

          return this.obtainFromCache;
     
    }

    /** Elimina el browser si fuera necesario. Este se llama al acabar la tarea de crawling */
    async destroy(){
        if(this.browser)
            await this.browser.close()
    }

    /**
     * Hace el crawling del site completo.
     * Si encuentra info en la cache la devuelve, sino utliza launch(url) para ir obteniendo links 
     * e in cargando la cache y luego ir recorriendola para asi visitar todo el site
     */
    async doCraw(){
        
        let filter= this.url.replace(/\//gi,"\\/")+"*";
        let result=[];
        if(!this.obtainFromCache){
            console.log("no habia en cache")
            //Se inicia el browser ya que vamos a leer de este
            this.browser=await puppeteer.launch(Crawler.PUPPETEER_OPTS);
            await this.launch(this.url);
            console.log("obtenido de web");
            result=await ResultCache.find({main_url:this.url,search:this.token, visited:false});
          
            //console.log(filter);
          try{ 
        
            while(result.length!=0){
           
             console.log("consulta");
            //console.log(result);
            for(var i in result){
                let url=result[i].url;
                await this.launch(url);
                await ResultCache.findByIdAndUpdate({_id:result[i]._id},{visited:true})
                //await ResultCache.findAndModify({_id:result[i]._id},{visited:true})

            }
            result=await ResultCache.find({main_url:this.url,search:this.token, visited:false});
        }
            }catch(e){
                console.log("ERROR")
                console.log(e);
               
            }


        result=await ResultCache.find({main_url:this.url,search:this.token},{_id:0,main_url:0,search:0,date:0,visited:0,__v:0});
           
        }else
       
        result=await ResultCache.find({main_url:this.url,search:this.token}, {_id:0,main_url:0,search:0,date:0,visited:0,__v:0});
        return result;

    }
 

    
    /**
     * Lanza una url y busca los links (relacionados o no con token de busqueda) guardandolos en caché
     * También marcará y actualizará el numero de ocurrencias del mismo link en la web
     * @param {String} url 
     */
    async launch(url){
        if(!url.startsWith("http"))
            return
     //Nos aseguramos que no se sale de la pagina
     let domainName=this.url.replace("www.","").replace("https://","").replace("http://","")       

     if(url.indexOf(domainName)==-1)
            return 
            
        console.log(`WebCrawling sobre ${url} ...`);
        let nodes={}
        try{
            //const browser=await puppeteer.launch(Crawler.PUPPETEER_OPTS);
            const page= await this.browser.newPage();

         /*   await page.setRequestInterception(true);
            page.on('request', request => {
              if (request.resourceType() === 'script')
                request.abort();
              else
                request.continue();
            });*/


            page.waitForNavigation();
            await page.goto(url);
        
            await page.setViewport({ width: 1600, height: 803 })
            nodes= await page.evaluate(tokenDom => {
                    //debugger;
                    let result={};
                    let nodes= document.querySelectorAll('a');
                    nodes.forEach(node => {
                        if(node.parentElement.textContent.indexOf(tokenDom)!=-1 || tokenDom=='*'){
                            //console.log(node.parentElement.textContent);
                            if(result[node.href])
                                result[node.href].ocurrences++;
                            else
                                result[node.href]={token:tokenDom,text:node.parentElement.textContent, ocurrences:1};
                        }
                    })
                    return Promise.resolve(result);
            },this.token);
            //console.log(nodes);
           
            await page.close();
        }catch(e){
            console.error(e);
        }
        console.log(`Encontrados nuevos elementos a insertar`)
        await this.insertInCache(nodes);
       
    }



    async insertInCache(nodes){
        for(let elem in nodes){
            let resCache=new ResultCache(
                {url:elem,
                    main_url: this.url,
                    search:this.token,
                    description:nodes[elem].text,
                    ocurrences:nodes[elem].ocurrences,
                    date:new Date(),
                    visited: false
                });
            let rc=await ResultCache.findOne({url:elem});
            if(!rc)    
                await resCache.save();
            else
                await ResultCache.findByIdAndUpdate({_id:rc._id},{ocurrences:rc.ocurrences+nodes[elem].ocurrences});    
        }
    }


} 

