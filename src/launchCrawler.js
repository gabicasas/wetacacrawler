import Crawler from './crawler/index'
import mongoose from 'mongoose';

(async()=>{

 await    mongoose.connect('mongodb://localhost/wetacaCrawler')


 let url= process.argv[2];
 let token= process.argv[3];
 let crawler= new Crawler(url,token);
 await crawler.initialize();
 let response= await crawler.doCraw(url);
 console.log(response);
})()